package rotar;

public class Enigma {

    private LeftRotor leftRotor = new LeftRotor();
    private MidRotor medrotor = new MidRotor();
    private RightRotor rightRotor = new RightRotor();


    public char[] Encrypt(String plain)
    {
        char[] encrytedArray = new char[plain.length()];
        System.out.println("Encrypting");
        plain = plain.toUpperCase();
        char [] cypher = new char[10000];

        for(int i = 0; i < plain.length();i++)
        {
            cypher[i] = EncryptChar(plain.charAt(i));
            encrytedArray[i] = cypher[i];

        }
        return encrytedArray;

    }

    public void Decrypt(String cypher)
    {
        System.out.println("Decrypting");
        cypher = cypher.toUpperCase();
        char [] plaintxt = new char[10000];

        for(int i = 0; i < cypher.length();i++)
        {
            plaintxt[i] = DecryptChar(cypher.charAt(i));
        }

        leftRotor = new LeftRotor();
        medrotor = new MidRotor();
        rightRotor = new RightRotor();
    }

    public char EncryptChar(char c)
    {
        char ch;

        try {

            rightRotor.turn();

            ch = medrotor.charAt(rightRotor.indexOf(c));
            ch = leftRotor.charAt(medrotor.indexOf(ch));

        }
        catch(Exception e) {
            System.out.println("Warning, character not in alphabet |" + c + "|");
            return c;
        }

        if(leftRotor.turns()%27 == 0)
            medrotor.turn();

        return ch;
    }

    public char DecryptChar(char c)
    {
        System.out.println("Decrypting " + c);
        char ch;

        try {
            ch = medrotor.charAt(rightRotor.indexOf(c));
            ch = leftRotor.charAt(rightRotor.indexOf(ch));
        }
        catch(Exception e) {
            System.out.println("Warning, character not in alphabet |" + c +"|");
            return c;
        }

        leftRotor.turn();

        if(leftRotor.turns()%27 == 0)
            medrotor.turn();

        return ch;

    }
}
